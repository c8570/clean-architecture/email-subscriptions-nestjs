import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { CreateSubscriberDto } from './dto';
import { SubscriberServices } from './subscriber.services';

@Controller('subscribers')
export class SubscriberControllers {
  constructor(private readonly subscriber_services: SubscriberServices) {}

  /* Aside from using the @MessagePattern(), 
    we can also implement event-based communication. 
    This is fitting for cases in which we don’t want to wait for a response. 
  */
  @EventPattern({ cmd: 'add-subscriber' })
  addSubscriber(subscriber: CreateSubscriberDto) {
    return this.subscriber_services.addSubscriber(subscriber);
  }

  @MessagePattern({ cmd: 'get-all-subscribers' })
  getAllSubscribers() {
    return this.subscriber_services.getAllSubscribers();
  }
}
