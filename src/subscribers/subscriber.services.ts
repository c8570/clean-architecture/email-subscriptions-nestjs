import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSubscriberDto } from './dto';
import { SubscriberFactoryService } from './subscriber-factory.services';
import { Subscriber } from './subscriber.entity';

@Injectable()
export class SubscriberServices {
  constructor(
    @InjectRepository(Subscriber)
    private subscriber_repository: Repository<Subscriber>,
    private subscriber_factory: SubscriberFactoryService,
  ) {}

  async addSubscriber(subscriber: CreateSubscriberDto) {
    const new_subscriber =
      this.subscriber_factory.createSubscriberFactory(subscriber);
    const created_subscriber = this.subscriber_repository.create(
      new_subscriber,
    );
    await this.subscriber_repository.save(created_subscriber);
    return created_subscriber;
  }

  async getAllSubscribers() {
    return await this.subscriber_repository.find();
  }
}
