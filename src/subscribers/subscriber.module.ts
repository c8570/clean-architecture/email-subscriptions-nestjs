import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubscriberFactoryService } from './subscriber-factory.services';
import { SubscriberControllers } from './subscriber.controllers';
import { Subscriber } from './subscriber.entity';
import { SubscriberServices } from './subscriber.services';

@Module({
  imports: [TypeOrmModule.forFeature([Subscriber])],
  controllers: [SubscriberControllers],
  providers: [SubscriberServices, SubscriberFactoryService],
  exports: [],
})
export class SubscriberModule {}
