import { Injectable } from '@nestjs/common';
import { CreateSubscriberDto } from './dto';
import { Subscriber } from './subscriber.entity';

@Injectable()
export class SubscriberFactoryService {
  createSubscriberFactory(subscriber: CreateSubscriberDto) {
    const new_subscriber = new Subscriber();
    new_subscriber.email = subscriber.email;
    new_subscriber.name = subscriber.name;
    return new_subscriber;
  }
}
